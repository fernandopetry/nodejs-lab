var app = require('./config/server');

require('./app/routes/index')(app);

// app.get('/', function (req, res) {
//   res.render("index");
// });

app.get('/hello', function (req, res) {
  res.send('Hello World! ');
});

app.get('/name/:name', function (req, res) {
  res.status(200).json({ username: 'Flavio' ,date: Date.now() , name:req.params.name})
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});